package br.edu.fatec.exercicio1labv.services;

import br.edu.fatec.exercicio1labv.entities.Carro;

public interface ICarroService extends IGenericService<Carro, Carro, Long>{}
