package br.edu.fatec.exercicio1labv.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.edu.fatec.exercicio1labv.entities.Carro;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ICarroRepositoryTest {

	@Autowired
	private ICarroRepository carroRepository;
	
	@Test
	public void testSalvar() {
		Carro carro = new Carro.CarroBuilder().setNome("Fusca").setMarca("Chevrolet").setCor("Amarelo").build();
		carroRepository.save(carro);
		assertTrue("Deveria retornar um carro cadastrado.",carroRepository.findById(carro.getId()).isPresent());
	}

	@Test
	public void testRemover() {
		Carro carro = new Carro.CarroBuilder().setNome("Fusca").setMarca("Chevrolet").setCor("Amarelo").build();
		carroRepository.save(carro);
		carroRepository.delete(carro);
		assertFalse("Não deveria retornar um carro cadastrado.",carroRepository.findById(carro.getId()).isPresent());
	}

	@Test
	public void testFindById() {
		Carro carro = new Carro.CarroBuilder().setNome("Fusca").setMarca("Chevrolet").setCor("Amarelo").build();
		carroRepository.save(carro);
		assertTrue("Deveria retornar um carro cadastrado.",carroRepository.findById(carro.getId()).isPresent());
	}

	@Test
	public void testFindAll() {
		Carro carro = new Carro.CarroBuilder().setNome("Fusca").setMarca("Chevrolet").setCor("Amarelo").build();
		Carro carro2 = new Carro.CarroBuilder().setNome("Fit").setMarca("Honda").setCor("Cinza").build();
		carroRepository.save(carro);
		carroRepository.save(carro2);
		assertEquals(2,carroRepository.findAll().size());
	}
}