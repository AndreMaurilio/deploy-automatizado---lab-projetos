package br.edu.fatec.exercicio1labv.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.edu.fatec.exercicio1labv.entities.Carro;
import br.edu.fatec.exercicio1labv.services.ICarroService;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class CarroServiceImplTest {

	@Autowired
	private ICarroService carroService;
	
	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void testSalvar() {
		Carro carro = new Carro.CarroBuilder().setNome("Fusca").setMarca("Volkswagen").setCor("Preto").build();
		carroService.salvar(carro);
		assertTrue("Deveria retornar um carro cadastrado.",carroService.findById(carro.getId()).isPresent());
		assertEquals("Fusca", carro.getNome());
		assertEquals("Volkswagen", carro.getMarca());
		assertEquals("Preto", carro.getCor());
	}

	@Test
	public void testRemover() {
		Carro carro = new Carro.CarroBuilder().setNome("Fusca").setMarca("Chevrolet").setCor("Amarelo").build();
		carroService.salvar(carro);
		carroService.remover(carro);
		assertFalse("Não deveria retornar um carro cadastrado.",carroService.findById(carro.getId()).isPresent());
	}

	@Test
	public void testAtualizar() {
		Carro carro = new Carro.CarroBuilder().setNome("Fusca").setMarca("Volkwagen").setCor("Azul").build();
		Carro carro2 = new Carro.CarroBuilder().setNome("Fit").setMarca("Honda").setCor("Cinza").build();
		carroService.salvar(carro);	
		carroService.atualizar(carro.getId(), carro2);
		assertEquals(carro.getId(), carro2.getId());
		assertEquals("Fit", carro2.getNome());
		assertEquals("Honda", carro2.getMarca());
		assertEquals("Cinza", carro2.getCor());
	}

	@Test
	public void testFindById() {
		Carro carro = new Carro.CarroBuilder().setNome("Fusca").setMarca("Chevrolet").setCor("Amarelo").build();
		carroService.salvar(carro);
		assertTrue("Deveria retornar um carro cadastrado.",carroService.findById(carro.getId()).isPresent());
	}

	@Test
	public void testFindAll() {
		Carro carro = new Carro.CarroBuilder().setNome("Fusca").setMarca("Chevrolet").setCor("Amarelo").build();
		Carro carro2 = new Carro.CarroBuilder().setNome("Fit").setMarca("Honda").setCor("Cinza").build();
		carroService.salvar(carro);
		carroService.salvar(carro2);
		assertEquals(2,carroService.findAll().size());
	}

}
